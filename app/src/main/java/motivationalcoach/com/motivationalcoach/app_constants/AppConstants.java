package motivationalcoach.com.motivationalcoach.app_constants;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mh on 2017-06-25.
 */

public class AppConstants {
    public static final String BODY_CALCULATOR = "BODY CALCULATOR";
    private AppConstants(){

    }

    public static String generateTimestamp() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timestamp = dateFormat.format(new Date());
        return timestamp;
    }

}
