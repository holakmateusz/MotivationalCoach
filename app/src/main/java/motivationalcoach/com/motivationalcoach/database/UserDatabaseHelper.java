package motivationalcoach.com.motivationalcoach.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import motivationalcoach.com.motivationalcoach.R;
import motivationalcoach.com.motivationalcoach.exception.ExceptionOfProject;
import motivationalcoach.com.motivationalcoach.view.activity.user_registration.BodyParameters;
import motivationalcoach.com.motivationalcoach.view.activity.user_registration.Sex;
import motivationalcoach.com.motivationalcoach.view.activity.user_registration.User;
import motivationalcoach.com.motivationalcoach.view.activity.user_registration.UserDataSingleton;

/**
 * Created by mh on 2017-06-23.
 */

public class UserDatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "MotivationalCoachDB";
    private static final int DB_VERSION = 3;
    private static final String DB_LOGGER = "UserDatabaseHelper";

    //TABLE USER
    private static final String DB_USER = "USER";
    private static final String DB__USER_NAME = "NAME";
    private static final String DB_USER_PASSWORD = "PASSWORD";
    private static final String DB_USER_SEX = "SEX";
    private static final String DB_USER_ID = "_id";

    //TABLE BODY_PARAMETERS
    private static final String DB_BODY_PARAMETERS = "BODY_PARAMETERS";
    private static final String DB_BODY_PARAMETERS_WEIGHT = "WEIGHT";
    private static final String DB_BODY_PARAMETERS_HEIGHT = "HEIGHT";
    private static final String DB_BODY_PARAMETERS_AGE = "AGE";
    private static final String DB_BODY_PARAMETERS_HIP_RATIO = "HIP_RATIO";
    private static final String DB_BODY_PARAMETERS_WAIST_CIRCUM = "WAIST_CIRCUM";
    private static final String DB_BODY_PARAMETERS_ID_USER = "id_user";
    private static final String DB_BODY_PARAMETERS_TIME = "TIME";

    public UserDatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        updateMyDatabase(db,0,DB_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        updateMyDatabase(db,oldVersion,newVersion);
    }
    
    private void updateMyDatabase(SQLiteDatabase db,int oldVersion,int newVersion){

        if(oldVersion<1){
            db.execSQL("CREATE TABLE USER(" +
                    "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "NAME TEXT," +
                    "PASSWORD TEXT" +
                    "SEX TEXT"+
                    ")");

            db.execSQL("CREATE TABLE BODY_PARAMETERS(" +
                    "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "WEIGHT REAL," +
                    "HEIGHT REAL," +
                    "AGE INTEGER," +
                    "HIP_RATIO REAL," +
                    "WAIST_CIRCUM REAL," +
                    "id_user INTEGER," +
                    "TIME text," +
                    "FOREIGN KEY(id_user) REFERENCES USER(_id))");
            //foreign key to enforce relations between USER table and BODY_PARAMETERS table
        }
        if(oldVersion<2){
            //// update table if database exist with version=1
            db.execSQL("ALTER TABLE USER " +
                    "ADD COLUMN SEX TEXT AFTER PASSWORD");
        }

        if(oldVersion==2){
            db.execSQL("ALTER TABLE BODY_PARAMETERS " +
                    "ADD COLUMN TIME TEXT" +
                    " AFTER id_user");
        }

    }

    public static BodyParameters getBodyParameters(SQLiteDatabase db,int idForeignKey){
        BodyParameters bodyParameters = null;
        Cursor cursor = db.query(DB_BODY_PARAMETERS,
                new String[] {
                                DB_BODY_PARAMETERS_WEIGHT,
                                DB_BODY_PARAMETERS_HEIGHT,
                                DB_BODY_PARAMETERS_AGE,
                                DB_BODY_PARAMETERS_HIP_RATIO,
                                DB_BODY_PARAMETERS_WAIST_CIRCUM,
                                DB_BODY_PARAMETERS_ID_USER,
                                DB_BODY_PARAMETERS_TIME
                },
                DB_BODY_PARAMETERS_ID_USER+"= ?",
                new String[]{Integer.toString(idForeignKey)},null,null,DB_BODY_PARAMETERS_TIME+" DESC LIMIT 1");
        if(cursor.moveToFirst()){
            bodyParameters = new BodyParameters(cursor.getDouble(0),
                                                cursor.getDouble(1),
                                                cursor.getInt(2),
                                                cursor.getDouble(3),
                                                cursor.getDouble(4));
        }
        cursor.close();
        return bodyParameters;
    }

    public static long insertNewBodyParameters(SQLiteDatabase db,BodyParameters bodyParameters,int idForeignKey){
        ContentValues bodyValues = new ContentValues();
        bodyValues.put(DB_BODY_PARAMETERS_WEIGHT,bodyParameters.getWeight());
        bodyValues.put(DB_BODY_PARAMETERS_HEIGHT,bodyParameters.getHeight());
        bodyValues.put(DB_BODY_PARAMETERS_AGE,bodyParameters.getAge());
        bodyValues.put(DB_BODY_PARAMETERS_HIP_RATIO,bodyParameters.getWaistHipRatio());
        bodyValues.put(DB_BODY_PARAMETERS_WAIST_CIRCUM,bodyParameters.getWaistCircumference());
        bodyValues.put(DB_BODY_PARAMETERS_ID_USER,idForeignKey);
        bodyValues.put(DB_BODY_PARAMETERS_TIME,bodyParameters.getTime());
        long result = db.insert(DB_BODY_PARAMETERS,null,bodyValues);
        db.close();
        return result;
    }

    public static long insertNewUser(User user,SQLiteDatabase db){
        long result=0;
        try{
            ContentValues userValues = new ContentValues();
            userValues.put(DB__USER_NAME,user.getUserName());
            userValues.put(DB_USER_PASSWORD,user.getPassword());
            userValues.put(DB_USER_SEX,user.getSex().toString());
            result = db.insert(DB_USER,null,userValues);
            if(result!=-1){
                UserDataSingleton userDataSingleton = UserDataSingleton.getUniqueInstance();
                if(userDataSingleton!=null){
                    UserDatabaseHelper.getUserData(user,db);

                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return result;
    }

    public static boolean checkUserExist(User user, SQLiteDatabase db,Context context){
        boolean state = false;
        Cursor cursor = db.query(DB_USER,new String[] {DB__USER_NAME,DB_USER_PASSWORD,DB_USER_SEX},
                                                        DB__USER_NAME+"= ?",new String[]{user.getUserName()},null,null,null);
        if(cursor.moveToFirst()){
            String name = cursor.getString(0);
            Toast toast = Toast.makeText(context,context.getString(R.string.user_already_exist),Toast.LENGTH_SHORT);
            toast.show();
            state = true;
        }
        cursor.close();
        return state;
    }



    public static boolean getUserData(User user,SQLiteDatabase db){
        boolean state = false;
        Cursor cursor = db.query(DB_USER,new String[] {DB_USER_ID,DB__USER_NAME,DB_USER_PASSWORD,DB_USER_SEX},
                                                        DB__USER_NAME+"= ?" +"AND "+DB_USER_PASSWORD+" = ?",
                                                        new String[]{user.getUserName(),user.getPassword()},null,null,null);
        if(cursor.moveToFirst()){
            int id = cursor.getInt(0);
            UserDataSingleton userDataSingleton = UserDataSingleton.getUniqueInstance();
            userDataSingleton.setIdLoggedInUser(id);
            String sex = cursor.getString(3);
            Sex sexEnum = Sex.valueOf(sex);
            userDataSingleton.setSex(sexEnum);
            state = true;
        }
        cursor.close();
        db.close();

        return state;
    }




    public static void QueryState(long result, Context context, String successResult, String errorResult){
        if(result!=-1){
            Toast toast = Toast.makeText(context,successResult,Toast.LENGTH_SHORT);
            toast.show();
        }else{
            Toast toast = Toast.makeText(context,errorResult,Toast.LENGTH_SHORT);
            toast.show();
        }
    }

}
