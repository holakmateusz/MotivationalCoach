package motivationalcoach.com.motivationalcoach;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import motivationalcoach.com.motivationalcoach.exception.ExceptionOfProject;
import motivationalcoach.com.motivationalcoach.view.activity.BMICategoryActivity;
import motivationalcoach.com.motivationalcoach.view.activity.CollectResults;
import motivationalcoach.com.motivationalcoach.view.activity.user_registration.Sex;
import motivationalcoach.com.motivationalcoach.view.activity.user_registration.UserDataSingleton;
import motivationalcoach.com.motivationalcoach.view.exercise_test.ExerciseActivity;

public class MainActivity extends Activity {
    private static final String MAIN_ACTIVITY_TAG = "MainActivity";
    private ListView listView;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = getIntent();
        initComponents();
        try {
            userId();
        } catch (ExceptionOfProject exceptionOfProject) {
            exceptionOfProject.printStackTrace();
        }


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //String itemText = (String) ((TextView) view).getText();
                openSubMenuActivity(position,getApplicationContext());
            }
        });
    }

    private void userId() throws ExceptionOfProject{
        UserDataSingleton userDataSingleton = UserDataSingleton.getUniqueInstance();
        int id  = userDataSingleton.getIdLoggedInUser();
        Sex sex = userDataSingleton.getSex();
        //Toast.makeText(getApplicationContext(),"id: "+id+" sex: "+sex.toString(),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // TODO animation while user click system button "back"
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }

    private void initComponents(){
        listView = (ListView)   findViewById(R.id.list_main_menu_options);
        textView = (TextView)   findViewById(R.id.textView);
    }

    private void openSubMenuActivity(int position, Context context){
        Intent intent;
        switch(position){
            case 0:
                //exercies
                intent = new Intent(context,ExerciseActivity.class);
                startActivity(intent);
                break;
            case 1:
                break;
            case 2:
                intent = new Intent(context,BMICategoryActivity.class);
                startActivity(intent);
                break;
            case 3:
                intent = new Intent(context,CollectResults.class);
                startActivity(intent);
                break;
            default:
                // TODO default
                break;
        }
    }



}
