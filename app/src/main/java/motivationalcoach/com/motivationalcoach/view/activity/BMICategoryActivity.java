package motivationalcoach.com.motivationalcoach.view.activity;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import motivationalcoach.com.motivationalcoach.R;
import motivationalcoach.com.motivationalcoach.app_constants.AppConstants;
import motivationalcoach.com.motivationalcoach.bmi_calculators.BodyCalculator;

public class BMICategoryActivity extends ListActivity {

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {

        Intent intent = null;
        switch((int)id){
            case 0:
                intent = new Intent(BMICategoryActivity.this,BMICalculatorActivity.class);
                break;
            case 1:
                intent = new Intent(BMICategoryActivity.this,WHRCalculatorActivity.class);
                break;
            case 2:
                intent = new Intent(BMICategoryActivity.this,BAICalculatorActivity.class);
                break;
            default:
                break;
        }
        intent.putExtra(AppConstants.BODY_CALCULATOR,(int) id);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ListView BMIcalculators = getListView();
        ArrayAdapter<BodyCalculator> listAdapter = new ArrayAdapter<BodyCalculator>(this,android.R.layout.simple_list_item_1, BodyCalculator.bmiCalculators);
        BMIcalculators.setAdapter(listAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // TODO animation while user click system button "back"
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }


}
