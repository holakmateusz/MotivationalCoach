package motivationalcoach.com.motivationalcoach.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import motivationalcoach.com.motivationalcoach.MainActivity;
import motivationalcoach.com.motivationalcoach.R;
import motivationalcoach.com.motivationalcoach.database.UserDatabaseHelper;
import motivationalcoach.com.motivationalcoach.view.activity.user_registration.BodyParameters;
import motivationalcoach.com.motivationalcoach.view.activity.user_registration.UserDataSingleton;

public class CollectResults extends Activity implements View.OnClickListener{

    private EditText tv_height;
    private EditText tv_weight;
    private EditText tv_age;
    private EditText tv_waistCircumference;
    private EditText tv_waistHipRatio;
    private Button saveDataInDB;
    private RadioGroup radioGroup;

    private void initialization(){
        tv_height = (EditText) findViewById(R.id.tvHeight);
        tv_weight = (EditText) findViewById(R.id.tvWeight);
        tv_age = (EditText) findViewById(R.id.tvAge);
        tv_waistCircumference = (EditText) findViewById(R.id.tvWaistCircumference);
        tv_waistHipRatio = (EditText) findViewById(R.id.tvWaistHipRatio);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroupSystemMeasurement);
        saveDataInDB = (Button) findViewById(R.id.btnSaveParameters);
        saveDataInDB.setOnClickListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collect_results);
        Intent intent = getIntent();
        initialization();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // TODO animation while user click system button "back"
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }

    private boolean checkDataIfFilled(){
        boolean state = true;
        String message = "";

//        if(radioGroup.getCheckedRadioButtonId()==-1){
//            message = (getString(R.string.error_field_required).toString())+" "+(getString(R.string.prompt_sex).toString());
//            state = false;
//            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
//        }
        String height = tv_height.getText().toString();
        if(TextUtils.isEmpty(height)){
            state = false;
            tv_height.setError(getString(R.string.error_field_required).toString());
        }
        String weight = tv_weight.getText().toString();
        if(TextUtils.isEmpty(weight)){
            state = false;
            tv_weight.setError(getString(R.string.error_field_required).toString());
        }

        String age = tv_age.getText().toString();
        if(TextUtils.isEmpty(age)){
            state = false;
            tv_age.setError(getString(R.string.error_field_required).toString());
        }

        String waist = tv_waistCircumference.getText().toString();
        if(TextUtils.isEmpty(waist)){
            state = false;
            tv_waistCircumference.setError(getString(R.string.error_field_required).toString());
        }

        String hip = tv_waistHipRatio.getText().toString();
        if(TextUtils.isEmpty(hip)){
            state = false;
            tv_waistHipRatio.setError(getString(R.string.error_field_required).toString());
        }
        return state;
    }



    private SQLiteDatabase connectWithDB(){
        SQLiteDatabase db = null;
        try{
            SQLiteOpenHelper userDatabaseHelper = new UserDatabaseHelper(this);
            db = userDatabaseHelper.getWritableDatabase();
        }catch (SQLiteException e){
            e.printStackTrace();
        }
        return db;
    }

    private BodyParameters parseBodyParameters(){
        double height;
        double weight;
        int age;
        double waistCircumference;
        double waistHipRatio;
        height = Double.parseDouble(tv_height.getText().toString());
        weight = Double.parseDouble(tv_weight.getText().toString());
        age = Integer.parseInt(tv_age.getText().toString());
        waistCircumference = Double.parseDouble(tv_waistCircumference.getText().toString());
        waistHipRatio = Double.parseDouble(tv_waistHipRatio.getText().toString());
        BodyParameters bodyParameters = new BodyParameters(weight,height,age,waistCircumference,waistHipRatio);
        return bodyParameters;
    }




    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSaveParameters:
                // TODO INSERT ROW IN DB IF DATA ARE PROPERLY, HAS ACCESS TO FOREIGN_KEY
                if(checkDataIfFilled()){
                    SQLiteDatabase db = connectWithDB();
                    if(db!=null){
                        UserDataSingleton userDataSingleton = UserDataSingleton.getUniqueInstance();
                        if(userDataSingleton!=null){
                            int idForeignKey = userDataSingleton.getIdLoggedInUser();
                            BodyParameters bodyParameters = parseBodyParameters();
                            long result = UserDatabaseHelper.insertNewBodyParameters(db,bodyParameters,idForeignKey);
                            UserDatabaseHelper.QueryState(result,getApplicationContext(),
                                                getString(R.string.database_insert_success).toString(),
                                                getString(R.string.database_insert_error).toString());
                            if(result!=-1){
                                Intent intent = new Intent(CollectResults.this,MainActivity.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
                            }
                        }
                    }
                }
                break;
            default:
                break;
        }
    }
}
