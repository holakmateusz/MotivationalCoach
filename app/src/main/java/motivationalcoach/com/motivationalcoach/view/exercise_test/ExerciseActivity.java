package motivationalcoach.com.motivationalcoach.view.exercise_test;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import motivationalcoach.com.motivationalcoach.R;

public class ExerciseActivity extends Activity{
    String[] descriptionArray = {
            "1. Przysiady ze sztangą za głową na maszynie Smitha 4 serie 15/12/12/12 powtórzeń \n"+
                    "Pozycja wyjściowa: \n"+
                    "Proste plecy i odcinek lędźwiowy.\n" +
                    "Sztanga trzymana na karku.\n" +
                    "Mocno napięte mięśnie brzucha.\n" +
                    "Stopy mniej więcej na szerokości barków (palce stóp na zewnątrz).\n" +
                    "Dłonie na sztandze w szerokim rozstawie.\n" +
                    "Klatka piersiowa wypchnięta do przodu.\n" +
                    "Stopy wysunięte lekko do przodu.\n",
                    "Ruch:\n" +
                    "Wykonuj płynny i powolny przysiad utrzymując naturalną krzywiznę kręgosłupa, do pozycji około kąta prostego, wraz z powolnym wdechem powietrza.\n" +
                    "Wykonując wydech, wstań do pozycji wyjściowej. Nie wykonuj przeprostu w stawach kolanowych.\n" +
                    "Powtarzaj ruch wyznaczoną ilość razy.",
            "2. Wypychanie ciężaru na suwnicy 3 serie 15 powtórzeń \n"+
                    "Pozycja wyjściowa:\n" +
                    "Usiądź w siedzisku suwnicy.\n" +
                    "Plecy oraz głowa cały czas przylegają do oparcia maszyny.\n" +
                    "Dłonie z boku na uchwytach maszyny.\n" +
                    "Nogi nieco szerzej od szerokości barków.\n" +
                    "Palce stóp skierowane lekko na zewnątrz.\n" +
                    "\n",
                    "Ruch:\n" +
                    "Utrzymując prawidłową pozycję wyjściową, wykonuj powolne opuszczenie ciężaru w dół, mniej więcej do kąta prostego w stawie kolanowym, pamiętając o wdechu powietrza.\n" +
                    "Wykonując wydech, prostuj stawy kolanowe, wypychając ciężar, jednak bez przeprostu w kolanach.\n" +
                    "Powtarzaj ruch wyznaczoną ilość razy.",
            "3. Przysiady ze sztangą na barkach 4 serie 12/10/10/10 powtórzeń \n"+
                    "Pozycja wyjściowa:\n" +
                    "Proste plecy i odcinek lędźwiowy.\n" +
                    "Sztanga z przodu na klatce piersiowej.\n" +
                    "Mocno napięte mięśnie brzucha.\n" +
                    "Stopy mniej więcej na szerokości barków (palce stóp na zewnątrz).\n" +
                    "Ręce splecione przed sobą trzymające gryf nachwytem.\n" +
                    "Klatka piersiowa wypchnięta do przodu.\n" +
                    "\n",
                    "Ruch:\n" +
                    "Utrzymując prawidłową pozycję wyjściową, wykonuj płynny i powolny przysiad utrzymując naturalną krzywiznę kręgosłupa, do pozycji mniej więcej kąta prostego, wraz z powolnym wdechem powietrza.\n" +
                    "Wykonując wydech, wstań do pozycji wyjściowej. Nie wykonuj przeprostu w stawach kolanowych.\n" +
                    "Powtarzaj ruch wyznaczoną ilość razy.",
            "4.Zginanie nóg na maszynie siedząc 4 serie 15 powtórzeń \n"+
                    "Pozycja wyjściowa:\n" +
                    "Zajmij miejsce na maszynie, dostosowując ją pod swój wzrost.\n" +
                    "Kończyny dolne wyprostowane, wałek maszyny znajduje się kilka centymetrów poniżej łydek.\n" +
                    "Chwyć za uchwyty znajdujące się po bokach siedziska.\n" +
                    "\n",
                    "Ruch:\n" +
                    "Wraz z wydechem powietrza ugnij kończyny dolne przeciągając wałek maszyny jak najdalej pod uda.\n" +
                    "Utrzymuj tułów nieruchomo, postaraj się utrzymać nogi w pozycji maksymalnego napięcia mięśni przez ułamek sekundy.\n" +
                    "Następnie wraz z wdechem powolnym tempem wróć do pozycji wyjściowej kontrolując ruch.\n" +
                    "Powtarzaj ruch wyznaczoną ilość razy.",
            "5.Wyprosty kolan na maszynie siedząc 5 serie 15 powtórzeń \n"+
                    "Pozycja wyjściowa:\n" +
                    "Siądź na siedzisku.\n" +
                    "Dłonie z boku na uchwytach maszyny.\n" +
                    "Proste plecy.\n" +
                    "\n",
                    "Ruch:\n" +
                    "Utrzymując prawidłową pozycję wyjściową, wykonuj dynamiczny wyprost kolan, jednak bez przeprostu (stałe napięcie mięśnia). Ruch wykonuj wraz z wydechem powietrza.\n" +
                    "Wykonując wdech, uginaj powolnym ruchem kolana do pozycji wyjściowej.\n" +
                    "Powtarzaj ruch wyznaczoną ilość razy.",
            "6. Wykroki z hantlami 4 serie 20 powtórzeń na stronę"+
                    "Pozycja wyjściowa:\n" +
                    "Proste plecy i odcinek lędźwiowy.\n" +
                    "Hantle trzymane chwytem młotkowym.\n" +
                    "Mocno napięte mięśnie brzucha.\n" +
                    "Stopy mniej więcej na szerokości barków (palce stóp na zewnątrz).\n" +
                    "Klatka piersiowa wypchnięta do przodu.\n" +
                    "\n",
                    "Ruch:\n" +
                    "Utrzymując prawidłową pozycję wyjściową, wykonuj wykrok jedną nogą do przodu utrzymując naturalną krzywiznę kręgosłupa tak, aby staw kolanowy nogi wykrocznej był zgięty w kącie około 90 stopni. Pamiętaj o wdechu powietrza. Noga zakroczna pozostaje w miejscu, przechodząc na palce podczas wykonywania wykroku.\n" +
                    "Wykonując wydech, wstań dynamicznie do pozycji wyjściowej.\n" +
                    "Powtarzaj ruch wyznaczoną ilość razy, zmieniając nogę wykroczną.",
            "7.  Wspięcia na palce z hantlami w dłoni 5 serii 25 powtórzeń"+
                    "Pozycja wyjściowa:\n" +
                    "W pozycji stojącej, plecy wyprostowane, ramiona wzdłuż tułowia, trzymają sztangielki.\n" +
                    "\n" +
                    "\n",
                    "Ruch:\n" +
                    "Z pozycji, w której stopa jest mocno zadarta do góry, pięta skrajnie obniżona, palce wskazują sufit a łydka mocno rozciągnięta, odpychaj się od podwyższenia poprzez mocne wspięcie na palce i napięcie łydek.\n" +
                    "Po krótkim przytrzymaniu w maksymalnym napięciu łydki, opuszczaj pięty do poziomu palców ponownie rozciągając łydkę i wykonując wydech.\n" +
                    "Powtarzaj ruch wyznaczoną ilość razy.",
            "8. Wspięcia na palce na maszynie 5x 10-15"+
                    "Pozycja wyjściowa:\n" +
                    "Usiądź na siedzisku maszyny do łydek.\n" +
                    "Oprzyj przednią część stopy na platformie.\n" +
                    "Unieś lekko pięty ku górze tak, aby możliwe było odciągnięcie dźwigni zwalniającej ciężar.\n" +
                    "\n",
                    "Ruch:\n" +
                    "Z pozycji, w której stopa jest mocno zadarta do góry, pięta skrajnie obniżona, palce wskazują sufit a łydka mocno rozciągnięta, odpychaj się od platformy poprzez mocne wspięcie na palce i napięcie łydek.\n" +
                    "Po krótkim przytrzymaniu w maksymalnym napięciu łydki, opuszczaj pięty pod poziom palców ponownie rozciągając łydkę i wykonując wydech.\n" +
                    "Powtarzaj ruch wyznaczoną ilość razy."
    };


    int[] imagesArray ={R.drawable.cwiczenie_1a,R.drawable.cwiczenie_1b,
                        R.drawable.cwiczenie_2a,R.drawable.cwiczenie_2b,
                        R.drawable.cwiczenie_3a,R.drawable.cwiczenie_3b,
                        R.drawable.cwiczenie_4a,R.drawable.cwiczenie_4b,
                        R.drawable.cwiczenie_5a,R.drawable.cwiczenie_5b,
                        R.drawable.cwiczenie_6a,R.drawable.cwiczenie_6b,
                        R.drawable.cwiczenie_7a,R.drawable.cwiczenie_7b,
                        R.drawable.cwiczenie_8a,R.drawable.cwiczenie_8b
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise);
        ListView listView = (ListView) findViewById(R.id.listView);

        CustomAdapter customAdapter  = new CustomAdapter();
        listView.setAdapter(customAdapter);
    }


    class CustomAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return imagesArray.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.list_item_exercise,null);
            ImageView imageView = (ImageView) convertView.findViewById(R.id.imageExercise);
            TextView text = (TextView) convertView.findViewById(R.id.TextExercise);
            imageView.setImageResource(imagesArray[position]);
            text.setText(descriptionArray[position]);
            return  convertView;
        }
    }


}
