package motivationalcoach.com.motivationalcoach.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import motivationalcoach.com.motivationalcoach.R;
import motivationalcoach.com.motivationalcoach.app_constants.AppConstants;
import motivationalcoach.com.motivationalcoach.bmi_calculators.BodyCalculator;
import motivationalcoach.com.motivationalcoach.bmi_calculators.ResearchResult;
import motivationalcoach.com.motivationalcoach.database.UserDatabaseHelper;
import motivationalcoach.com.motivationalcoach.view.activity.user_registration.BodyParameters;
import motivationalcoach.com.motivationalcoach.view.activity.user_registration.PublishResultInterface;
import motivationalcoach.com.motivationalcoach.view.activity.user_registration.Sex;
import motivationalcoach.com.motivationalcoach.view.activity.user_registration.UserDataSingleton;

public class WHRCalculatorActivity extends Activity implements View.OnClickListener,PublishResultInterface {
    private Button btnCountWHR;
    private TextView tvDisplayTopTitle;
    private TextView resultWHR;
    private TextView answerWHR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whrcalculator);
        init();
        Intent intent = getIntent();
        int bmiItem = intent.getIntExtra(AppConstants.BODY_CALCULATOR,0);
        BodyCalculator bmiCalculator = BodyCalculator.bmiCalculators[bmiItem];
        tvDisplayTopTitle.setText(bmiCalculator.toString());
    }

    private void init(){
        resultWHR = (TextView) findViewById(R.id.whr_result);
        answerWHR = (TextView) findViewById(R.id.whr_information);
        tvDisplayTopTitle = (TextView) findViewById(R.id.whr_calculator);
        btnCountWHR = (Button) findViewById(R.id.btnCountWHR);
        btnCountWHR.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // TODO animation while user click system button "back"
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnCountWHR:
                try {
                    SQLiteOpenHelper userDatabaseHelper = new UserDatabaseHelper(this);
                    SQLiteDatabase db = userDatabaseHelper.getReadableDatabase();
                    BodyParameters bodyParameters;
                    UserDataSingleton userDataSingleton = UserDataSingleton.getUniqueInstance();
                    if(userDataSingleton!=null){
                        bodyParameters = UserDatabaseHelper.getBodyParameters(db,userDataSingleton.getIdLoggedInUser());
                        if(bodyParameters!=null){
                            ResearchResult researchResult;
                            Sex userSex = userDataSingleton.getSex();
                            researchResult = BodyCalculator.calculateWHR(bodyParameters,userSex,getApplicationContext());
                            publishResult(researchResult.getResult(),researchResult.getInformationForUser());
                        }
                    }
                }catch(SQLiteException e){
                    Toast toast = Toast.makeText(getApplicationContext(),getString(R.string.database_error).toString(),Toast.LENGTH_SHORT);
                    toast.show();
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void publishResult(double result, String information) {
        resultWHR.setText(String.valueOf(result));
        answerWHR.setText(information);
    }


}
