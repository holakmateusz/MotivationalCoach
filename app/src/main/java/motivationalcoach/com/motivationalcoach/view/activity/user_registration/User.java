package motivationalcoach.com.motivationalcoach.view.activity.user_registration;

/**
 * Created by mh on 2017-06-22.
 */

public class User {
    private String userName;
    private String password;
    private Sex sex;




    public User(String userName, String password) {
        this.userName = userName;
        this.password = password;
        sex = Sex.UNDEFINED;
    }


    public User(String userName, Sex sex) {
        this.userName = userName;
        this.sex = sex;
        password = "";
    }

    public User(String userName, String password, Sex sex) {
        this.userName = userName;
        this.password = password;
        this.sex = sex;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }
}
