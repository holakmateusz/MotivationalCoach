package motivationalcoach.com.motivationalcoach.view.activity.user_registration;

/**
 * Created by mh on 2017-06-25.
 */

public interface PublishResultInterface {
     public abstract void publishResult(double result,String information);

}
