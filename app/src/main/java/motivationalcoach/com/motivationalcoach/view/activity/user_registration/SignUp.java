package motivationalcoach.com.motivationalcoach.view.activity.user_registration;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import motivationalcoach.com.motivationalcoach.MainActivity;
import motivationalcoach.com.motivationalcoach.R;
import motivationalcoach.com.motivationalcoach.database.UserDatabaseHelper;


public class SignUp extends Activity implements View.OnClickListener {

    private EditText userName;
    private EditText password;
    private EditText confirmPassword;
    private Button btnRegistration;
    private RadioGroup radioGroup;
    private RadioButton femRadioBtn;
    private RadioButton malRadioBtn;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        init();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // TODO animation while user click system button "back"
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }

    private void init(){
        userName = (EditText) findViewById(R.id.tvName);
        password = (EditText) findViewById(R.id.tvPassword);
        confirmPassword = (EditText) findViewById(R.id.tvConfirmPassword);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroupButton);
        femRadioBtn = (RadioButton) findViewById(R.id.radio_btn_female);
        malRadioBtn = (RadioButton) findViewById(R.id.radio_btn_male);
        btnRegistration = (Button) findViewById(R.id.btnSignUp);
        btnRegistration.setOnClickListener(this);
    }

    private User getUserData(){
        String user = userName.getText().toString();
        String pass = password.getText().toString();
        Sex sex = null;
        int selected = radioGroup.getCheckedRadioButtonId();
        RadioButton radioButton;
        radioButton = (RadioButton) findViewById(selected);
        if(selected==R.id.radio_btn_female){
            sex = Sex.FEMALE;
        }else if(selected==R.id.radio_btn_male){
           sex = Sex.MALE;
        }
        User newUser = new User(user,pass,sex);
        return newUser;
    }

    private boolean checkDataIfFilled(){
        boolean state = true;
        String message = "";

        if(radioGroup.getCheckedRadioButtonId()==-1){
            message = (getString(R.string.error_field_required).toString())+" "+(getString(R.string.prompt_sex).toString());
            state = false;
            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
        }

        String name = userName.getText().toString();
        if(TextUtils.isEmpty(name)){
            state = false;
            userName.setError(getString(R.string.error_field_required).toString());
        }
        String pass = password.getText().toString();
        if(TextUtils.isEmpty(pass)){
            state = false;
            password.setError(getString(R.string.error_field_required).toString());
        }

        String confirmPass = confirmPassword.getText().toString();
        if(TextUtils.isEmpty(pass)){
            state = false;
            confirmPassword.setError(getString(R.string.error_field_required).toString());
        }

        if(!(password.getText().toString().equals(confirmPassword.getText().toString()))){
            state = false;
            confirmPassword.setError(getString(R.string.error_incorrect_password).toString());
        }

       return state;
    }

    private void registerUser(){
        if(checkDataIfFilled()){
            User user = getUserData();
            try{
                SQLiteOpenHelper userDatabaseHelper = new UserDatabaseHelper(this);
                SQLiteDatabase db = userDatabaseHelper.getWritableDatabase();
                //// TODO: Add transaction
                boolean ifExist = UserDatabaseHelper.checkUserExist(user,db,getApplicationContext());
                if(!ifExist){
                    long result  = UserDatabaseHelper.insertNewUser(user,db);
                    UserDatabaseHelper.QueryState(result,getApplicationContext(),getString(R.string.user_added_success).toString(),getString(R.string.user_added_error).toString());
                    if(result!=-1){
                        Intent intent = new Intent(SignUp.this,MainActivity.class);
                        //intent.putExtra(BMICalculatorActivity.BMI_TAG,(int) id);
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                    }else{
                        Toast.makeText(getApplicationContext(),getString(R.string.database_error).toString(),Toast.LENGTH_SHORT).show();
                    }
                }
            }catch (SQLiteException e){
                Toast toast = Toast.makeText(getApplicationContext(),getString(R.string.database_error).toString(),Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSignUp:
                registerUser();
                break;
            default:
                break;
        }
    }



}
