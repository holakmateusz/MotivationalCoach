package motivationalcoach.com.motivationalcoach.view.activity.user_registration;

import motivationalcoach.com.motivationalcoach.app_constants.AppConstants;

/**
 * Created by mh on 2017-06-23.
 */

public class BodyParameters {
    private double weight;
    private double height;
    private int age;
    private double waistCircumference;
    private double waistHipRatio;
    private String time;


    public BodyParameters() {
        time = AppConstants.generateTimestamp();
    }

    public BodyParameters(double weight,double height, int age, double waistCircumference, double waistHipRatio) {
        this.weight = weight;
        this.height = height;
        this.age = age;
        this.waistCircumference = waistCircumference;
        this.waistHipRatio = waistHipRatio;
        time = AppConstants.generateTimestamp();
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getWaistCircumference() {
        return waistCircumference;
    }

    public void setWaistCircumference(double waistCircumference) {
        this.waistCircumference = waistCircumference;
    }

    public double getWaistHipRatio() {
        return waistHipRatio;
    }

    public void setWaistHipRatio(double waistHipRatio) {
        this.waistHipRatio = waistHipRatio;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
