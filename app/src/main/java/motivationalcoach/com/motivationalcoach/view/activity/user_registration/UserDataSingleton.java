package motivationalcoach.com.motivationalcoach.view.activity.user_registration;

import java.util.ArrayList;

/**
 * Created by mh on 2017-06-24.
 */

public class UserDataSingleton {
    private ArrayList<BodyParameters> listOfParameters;
    private int idLoggedInUser;
    private Sex sex;
    private volatile static UserDataSingleton uniqueInstance;

    private UserDataSingleton(){
        listOfParameters = new ArrayList<BodyParameters>();
    }

    public static UserDataSingleton getUniqueInstance(){
        if(uniqueInstance==null){
            synchronized (UserDataSingleton.class){
                if(uniqueInstance==null){
                    uniqueInstance = new UserDataSingleton();
                }
            }
        }
        return  uniqueInstance;
    }

    public int getIdLoggedInUser() {
        return idLoggedInUser;
    }

    public void setIdLoggedInUser(int idLoggedInUser) {
        this.idLoggedInUser = idLoggedInUser;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }
}


