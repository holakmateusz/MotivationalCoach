package motivationalcoach.com.motivationalcoach.view.activity.user_registration;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import motivationalcoach.com.motivationalcoach.MainActivity;
import motivationalcoach.com.motivationalcoach.R;
import motivationalcoach.com.motivationalcoach.database.UserDatabaseHelper;


public class LoginActivity extends Activity implements View.OnClickListener {

    private EditText userName;
    private EditText password;
    private Button btnSignIn;
    private Button btnSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
        //TODO DELETE SCHEMA !
        //deleteDatabase("MotivationalCoachDB");
    }

    private void init(){
        userName = (EditText) findViewById(R.id.tvName);
        password = (EditText) findViewById(R.id.tvPassword);
        btnSignIn = (Button) findViewById(R.id.btnSignIn);
        btnSignUp = (Button) findViewById(R.id.btnSignUp);
        btnSignIn.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);
    }


    private boolean checkTheFormIsEmpty(){
        boolean state = false;
        String name = userName.getText().toString();
        if(TextUtils.isEmpty(name)){
            state = true;
            userName.setError(getString(R.string.error_field_required).toString());
        }
        String pass = password.getText().toString();
        if(TextUtils.isEmpty(pass)){
            state = true;
            password.setError(getString(R.string.error_field_required).toString());
        }
        return state;
    }

    private void logIn(){
        String name = userName.getText().toString();
        String pass = password.getText().toString();
        User user = new User(name,pass);
        try{
            SQLiteOpenHelper userDatabaseHelper = new UserDatabaseHelper(this);
            SQLiteDatabase db = userDatabaseHelper.getReadableDatabase();
            boolean state = UserDatabaseHelper.getUserData(user,db);
            if(state){
                //TODO CREATE SINGLETON
                Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
            }else{
                Toast.makeText(getApplicationContext(),getString(R.string.error_invalid_user).toString(),Toast.LENGTH_SHORT).show();
            }
        }catch (SQLiteException e){
            Toast toast = Toast.makeText(getApplicationContext(),getString(R.string.database_error).toString(),Toast.LENGTH_SHORT);
            toast.show();
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()){
            case R.id.btnSignIn:
                if(!checkTheFormIsEmpty()){
                    logIn();
                }
                break;
            case R.id.btnSignUp:
                intent = new Intent(LoginActivity.this,SignUp.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                break;
            default:
                break;
        }
    }


}
