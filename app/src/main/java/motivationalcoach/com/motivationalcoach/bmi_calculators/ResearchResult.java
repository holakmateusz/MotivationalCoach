package motivationalcoach.com.motivationalcoach.bmi_calculators;

/**
 * Created by mh on 2017-06-25.
 */

public class ResearchResult {
    private String informationForUser;
    private double result;

    public ResearchResult(String informationForUser, double result) {
        this.informationForUser = informationForUser;
        this.result = result;
    }

    public String getInformationForUser() {
        return informationForUser;
    }

    public void setInformationForUser(String informationForUser) {
        this.informationForUser = informationForUser;
    }

    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        this.result = result;
    }
}
