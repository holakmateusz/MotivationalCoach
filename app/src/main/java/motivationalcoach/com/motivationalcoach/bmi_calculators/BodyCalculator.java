package motivationalcoach.com.motivationalcoach.bmi_calculators;

import android.content.Context;

import motivationalcoach.com.motivationalcoach.R;
import motivationalcoach.com.motivationalcoach.view.activity.user_registration.BodyParameters;
import motivationalcoach.com.motivationalcoach.view.activity.user_registration.Sex;

import static java.lang.Math.*;
/**
 * Created by mh on 2017-05-22.
 */

public class BodyCalculator {
    private String name;
    private static final double CHANGE_TO_METER = 100.0;


    private BodyCalculator(String name){
        this.name = name;
    }

    public static final BodyCalculator[] bmiCalculators = {
        new BodyCalculator("BMI"),new BodyCalculator("WHR"),new BodyCalculator("BIA")
    };

    public String toString(){
        return this.name;
    }

    public static ResearchResult calculateBMI(BodyParameters bodyParameters, Context context){
        double result;
        String answer;
        ResearchResult researchResult = null;
        result = bodyParameters.getWeight()/(pow(bodyParameters.getHeight()/CHANGE_TO_METER,2));
        if(result<bmiSection.underweight.section){
            researchResult = new ResearchResult(context.getString(R.string.bmi_underweight).toString(),result);
        }else if(result>bmiSection.overWeight.section){
            researchResult = new ResearchResult(context.getString(R.string.bmi_overWeight).toString(),result);
        }else{
            researchResult = new ResearchResult(context.getString(R.string.bmi_normal_weight).toString(),result);
        }
        return researchResult;
    }

    public static ResearchResult calculateWHR(BodyParameters bodyParameters, Sex sex, Context context){
        double result;
        String answer="";
        ResearchResult researchResult = null;
        result = bodyParameters.getWaistCircumference()/bodyParameters.getWaistHipRatio();
        if(sex.equals(Sex.FEMALE)){
            if(result>=0.85 || bodyParameters.getWaistCircumference()>88.0){
                answer = context.getString(R.string.whr_abdominal_obesity).toString();
            }
            if(result<0.85){
                answer = context.getString(R.string.whr_obesity_type_buttock_thigh).toString();
            }
        }
        if(sex.equals(Sex.MALE)){
            if(result>=1.0 || bodyParameters.getWaistCircumference()>102.0){
                answer = context.getString(R.string.whr_abdominal_obesity).toString();
            }
            if(result<1.0){
                answer = context.getString(R.string.whr_obesity_type_buttock_thigh).toString();
            }
            researchResult = new ResearchResult(answer,result);
        }
        return researchResult;
    }

    public static ResearchResult calculateBAI(BodyParameters bodyParameters,Context context){
        double result;
        String answer;
        ResearchResult researchResult = null;
        result = ((100.0*(bodyParameters.getWaistHipRatio()/100.0))/((bodyParameters.getHeight()/100.0)*sqrt(bodyParameters.getHeight()/100.0)))-18;
        researchResult = new ResearchResult(context.getString(R.string.bai_fat_percentege).toString(),result);
        return researchResult;

    }


    private enum bmiSection{
        underweight(18.5),overWeight(24.99);

        private double section;
        bmiSection(double section) {
            this.section = section;
        }

    }



}
