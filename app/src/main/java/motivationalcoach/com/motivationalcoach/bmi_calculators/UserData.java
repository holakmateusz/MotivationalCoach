package motivationalcoach.com.motivationalcoach.bmi_calculators;

/**
 * Created by mh on 2017-05-22.
 */

public class UserData {
    private String userName;
    private float weight;
    private float height;

    public static final UserData[] userData = {
      new UserData("Mateusz",52.3f,1.61f)
    };


    public UserData(String userName, float weight, float height) {
        this.userName = userName;
        this.weight = weight;
        this.height = height;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }
}
