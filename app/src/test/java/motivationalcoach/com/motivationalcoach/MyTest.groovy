import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import motivationalcoach.com.motivationalcoach.database.UserDatabaseHelper
import motivationalcoach.com.motivationalcoach.view.activity.user_registration.User
import spock.lang.Specification

class MyTest extends Specification{
    def "should register the user"() {
        given:
        User user;
        long result;
        when:
        user= new User("Mateusz","mojeHaslo");
        deleteDatabase("MotivationalCoachDB");
        try{
            SQLiteOpenHelper userDatabaseHelper = new UserDatabaseHelper(this);
            SQLiteDatabase db = userDatabaseHelper.getWritableDatabase();
            result  = UserDatabaseHelper.insertNewUser(user,db);
        }catch (SQLiteException e){
        }
        then:
        result !=-1
    }
}